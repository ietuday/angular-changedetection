import { TestBed, inject } from '@angular/core/testing';

import { ToggleClassService } from './toggle-class.service';

describe('ToggleClassService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ToggleClassService]
    });
  });

  it('should be created', inject([ToggleClassService], (service: ToggleClassService) => {
    expect(service).toBeTruthy();
  }));
});
