import { TestBed, inject } from '@angular/core/testing';

import { ToggleStateService } from './toggle-state.service';

describe('ToggleStateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ToggleStateService]
    });
  });

  it('should be created', inject([ToggleStateService], (service: ToggleStateService) => {
    expect(service).toBeTruthy();
  }));
});
